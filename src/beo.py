from beowulf.beowulfd import Beowulfd
from beowulf.commit import Commit

import asyncio
import math
import time
# from pprint import pprint as print

from task_queue import AsyncWaitTaskQueue2
tasks = AsyncWaitTaskQueue2()

# Client setup
s = Beowulfd()
c = Commit(beowulfd_instance=s, no_wallet_file=True)
public_key= "BEO5s81ykGY8dTudRvt9Xtd1EbzZQmrY8sdDC6TuwzNQLhWsP4rza",
pri_key= "5K7AxXPLQymHULcCPjTcHTuehmf7bokkVVEGtrxuYphZuPKFeov"

# Variances
new_account_name = "vothanhdat"
creator = "lamhai"

c.wallet.unlock("password")

# Transfer native coin
asset_bwf = "BWF"
asset_w = "W"
asset_tot = "TOT"
asset_fee = "W"
amount = "1.00000"
fee = "0.01000"

def getTimestamp():
    return math.trunc(time.time()*1000)


# Transfer BWF from creator to new_account_name
async def tranferBWF(future):
    a = c.transfer(account=creator, amount=amount, asset=asset_bwf, fee=fee, asset_fee=asset_fee, memo="", to=new_account_name)
    if a is not None:
        future.set_result("Task done at: {}".format(getTimestamp()))
async def tranferW(future):
    a = c.transfer(account=creator, amount=amount, asset=asset_w, fee=fee, asset_fee=asset_fee, memo="", to=new_account_name)
    if a is not None:
        future.set_result("Task done at: {}".format(getTimestamp()))
    

acc = c.wallet.getAccounts()
print("acc before",acc)

def result_callback(future):
    result = future.result()
    print(result)


async def main4():
    for i in range(0, 100000):
        tasks.add_to_queue(tranferBWF, result_callback)
        tasks.add_to_queue(tranferW, result_callback)
    await tasks.start()

print("start at: ", getTimestamp())
asyncio.run(main4())
print("done at: ", getTimestamp())

acc = c.wallet.getAccounts()
print("acc after",acc)