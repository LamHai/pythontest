import time
import threading
import asyncio

from concurrent.futures import ThreadPoolExecutor
from collections import deque

'''
AsyncTaskQueue: Queue of async tasks invoked automatically 
Example:
    queue = AsyncTaskQueue()
    queue.add_to_queue(async_task, result_callback, param)
'''
class AsyncTaskQueue:

    def __init__(self, thread_num=1):
        self.queue = deque()
        self.thread_num = thread_num
        self.threadExecutor = ThreadPoolExecutor(self.thread_num)
        threading.Thread(target=self.main).start()
        

    def to_list(self, *args):
        l = list()
        for a in args:
            l.append(a)
        return l

    def to_dict(self, **kwargs):
        d = dict()
        for key, value in kwargs.items():
            d[key] = value
        return d

    def add_to_queue(self, task, call_back, *args):
        # print('Add to queue: ', task, call_back, *args)
        self.queue.append((task, call_back, self.to_list(*args)))
        # print('Queue size: ', len(self.queue))
    
    def main(self):
        # print('Entering main queue: ')
        self.new_event_loop() # optional
        # iter = 0
        while True:
            # iter += 1
            # print('iter: ', iter, ' ', len(self.queue) )
            self.run_async_tasks()
            time.sleep(0.1)
        self.close_event_loop() # optional 

    
    def get_current_event_loop(self):
        self.event_loop = asyncio.get_event_loop()
        self.is_create_loop = False

    def new_event_loop(self):
        self.event_loop = asyncio.new_event_loop()
        self.is_create_loop = True

    def close_event_loop(self):
        if self.is_create_loop and self.event_loop is not None:
            self.event_loop.close()
            self.event_loop = None
            self.is_create_loop = False

    async def async_tasks(self, executor, tasks):
        # loop = asyncio.get_event_loop()
        loop = self.event_loop
        asyncio.set_event_loop(loop)
        if len(tasks) > 0:
            _async_tasks = []
            for t in tasks:   
                _future = asyncio.Future()           
                future = await loop.run_in_executor(executor, t[0], _future, *t[2])
                if t[1] is not None:
                    _future.add_done_callback(t[1])
                _async_tasks.append(future)    
            await asyncio.gather(*_async_tasks)

    def run_async_tasks(self):
        # print('Queue size: ', len(self.queue))
        if self.event_loop is None:
            self.get_current_event_loop()
        try:
            tasks = []
            while True:
                if len(self.queue)<=0:
                    break
                tasks.append(self.queue.popleft())
                if len(tasks) > 20:
                    break
            # print('tasks: ', len(tasks))
            if self.threadExecutor is not None:
                self.event_loop.run_until_complete(self.async_tasks(self.threadExecutor, tasks))
        finally:
            pass
        
'''
AsyncWaitTaskQueue: Queue of async tasks invoked manually 
Example:
    queue = AsyncWaitTaskQueue()
    queue.add_to_queue(async_task, result_callback, param)
    queue.add_to_queue(another_task, result_callback, param)
    queue.start()
'''
class AsyncWaitTaskQueue:

    def __init__(self, thread_num=1):
        self.queue = deque()
        self.thread_num = thread_num
        self.threadExecutor = ThreadPoolExecutor(self.thread_num)
        self.new_event_loop() # optional

    def to_list(self, *args):
        l = list()
        for a in args:
            l.append(a)
        return l

    def to_dict(self, **kwargs):
        d = dict()
        for key, value in kwargs.items():
            d[key] = value
        return d

    def add_to_queue(self, task, call_back, *args):
        # print('Add to queue: ', task, call_back, *args)
        self.queue.append((task, call_back, self.to_list(*args)))
        # print('Queue size: ', len(self.queue))
    
    def start(self):
        self.run_async_tasks()
        
    def get_current_event_loop(self):
        self.event_loop = asyncio.get_event_loop()
        self.is_create_loop = False

    def new_event_loop(self):
        self.event_loop = asyncio.new_event_loop()
        self.is_create_loop = True

    def close_event_loop(self):
        if self.is_create_loop and self.event_loop is not None:
            self.event_loop.close()
            self.event_loop = None
            self.is_create_loop = False

    async def async_tasks(self, executor, tasks):
        # loop = asyncio.get_event_loop()
        loop = self.event_loop
        asyncio.set_event_loop(loop)
        if len(tasks) > 0:
            _async_tasks = []
            for t in tasks:   
                _future = asyncio.Future()           
                future = await loop.run_in_executor(executor, t[0], _future, *t[2])
                if t[1] is not None:
                    _future.add_done_callback(t[1])
                _async_tasks.append(future)    
            await asyncio.gather(*_async_tasks)

    def run_async_tasks(self):
        # print('Queue size: ', len(self.queue))
        if self.event_loop is None:
            self.get_current_event_loop()
        try:
            tasks = []
            while True:
                if len(self.queue)<=0:
                    break
                tasks.append(self.queue.popleft())
            # print('tasks: ', len(tasks))
            if self.threadExecutor is not None:
                self.event_loop.run_until_complete(self.async_tasks(self.threadExecutor, tasks))
        finally:
            pass

class AsyncWaitTaskQueue2:

    def __init__(self):
        self.queue = deque()
        
    def to_list(self, *args):
        l = list()
        for a in args:
            l.append(a)
        return l

    def to_dict(self, **kwargs):
        d = dict()
        for key, value in kwargs.items():
            d[key] = value
        return d
    
    def add_to_queue(self, task, call_back, *args):
        # print('Add to queue: ', task, call_back, *args)
        self.queue.append((task, call_back, self.to_list(*args)))
        # print('Queue size: ', len(self.queue))


    async def start(self):
        # print('Queue size: ', len(self.queue))
        try:
            tasks = []
            i = 0
            while True and i < 20 and self.queue:
                i = i + 1
                task = self.queue.popleft()
                _future = asyncio.Future() 
                future = asyncio.create_task(task[0](_future, *task[2]))
                if task[1] is not None:
                    _future.add_done_callback(task[1])
                tasks.append(future)
            await asyncio.gather(*tasks)
            # print('tasks: ', len(tasks))
        finally:
            pass