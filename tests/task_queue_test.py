import time
import asyncio
from task_queue import AsyncTaskQueue, AsyncWaitTaskQueue2
import functools

async def async_task(future, *args):
    i = 1
    while i < 4:
        i = i + 1
        await asyncio.sleep(1)
        print("Task {}".format(args[0]))
    if future is not None and not future.cancelled():
        future.set_result("Result: Task {} complete".format(args[0]))
        args[1].cancel()

def result_callback(future):
    result = future.result()
    print(result)
        

def main(): 
    queue1 = AsyncTaskQueue()
    queue2 = AsyncTaskQueue()
    t = 0
    queue1.add_to_queue(async_task, result_callback, t)
    queue1.add_to_queue(async_task, result_callback, t+1)
    queue2.add_to_queue(async_task, result_callback, t+2)
    queue2.add_to_queue(async_task, result_callback, t+3)
    
    while True:
        t = t + 1
        print('main thread: ', t)
        if t % 4 == 0:
            queue1.add_to_queue(async_task, result_callback, t)
            queue1.add_to_queue(async_task, result_callback, t+1)
        elif t % 4 == 2:
            queue2.add_to_queue(async_task, result_callback, t)
            queue2.add_to_queue(async_task, result_callback, t+1)
        time.sleep(1)

def main2():
    queue = AsyncWaitTaskQueue2()
    t = 0
    queue.add_to_queue(async_task, result_callback, t)
    queue.add_to_queue(async_task, result_callback, t+1)
    queue.add_to_queue(async_task, result_callback, t+2)
    queue.add_to_queue(async_task, result_callback, t+3)
    queue.start()
    t = 5
    queue.add_to_queue(async_task, result_callback, t)
    queue.add_to_queue(async_task, result_callback, t+1)
    queue.add_to_queue(async_task, result_callback, t+2)
    queue.add_to_queue(async_task, result_callback, t+3)
    queue.start()



async def main_main():
    print('main start')
    try:
        while True:
            print('main running')
            await asyncio.sleep(1)
    except asyncio.CancelledError:
        print('main cancelled')

async def main3():

    main = asyncio.create_task(main_main())
    future = asyncio.get_event_loop().create_future()
    task = asyncio.create_task(async_task(future, 1, main))
    future.add_done_callback(result_callback)
    # task.add_done_callback(functools.partial(result_callback, future, main_task))
    await asyncio.gather(task, main)


async def main4_inner_inner(future, *args):
    print('main4 inner inner: ', args[0]," ", args[1])
    if future is not None and not future.cancelled():
        future.set_result("Result: Task {} {} complete".format(args[0], args[1]))

async def main4_inner(future, *args):
    print('main4 inner: ', args[0])
    waitqueue = AsyncWaitTaskQueue2()
    for j in range(2):
        waitqueue.add_to_queue(main4_inner_inner, result_callback, args[0], j)
        waitqueue.add_to_queue(main4_inner_inner, result_callback, args[0], j+1)
        await waitqueue.start()
        
    print('Main4 inner ', args[0], ' complete')
    if future is not None and not future.cancelled():
        future.set_result("Result: Task {} complete".format(args[0]))

async def main4():
    queue = AsyncTaskQueue()
    i = 0
    while True:
        i = i + 1
        queue.add_to_queue(main4_inner, result_callback, i)

asyncio.run(main4())