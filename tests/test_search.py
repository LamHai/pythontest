from search import search

def test_search():
    assert search(2, [1, 2, 3, 4]) == 1, 'found needle somewhere in the haystack'

def test_search_first_element():
    assert search(1, [1, 2, 3, 4]) == 0, 'search first element'

def test_search_last_element():
    assert search(4, [1, 2, 3, 4]) == 3, 'search last element'
    
def test_exception_not_found():
    from pytest import raises
    with raises(ValueError):
        search(-1, [1, 2, 3, 4])
    with raises(ValueError):
        search(5, [1, 2, 3, 4])
    with raises(ValueError):
        search(2, [1, 3, 4])

def test_with_mock():
    from unittest.mock import Mock, patch
    mock_requests = Mock()
    # Get function of mock object return value
    mock_requests.get.return_value.text = 'aa bb cc'
    # A attribute = 3
    mock_requests.A = 3