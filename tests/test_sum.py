from sum import func

def test_1():
    assert func(3) == 2

def test_2():
    assert func(4) == 3

def test_3():
    assert func(6) == 7