# Local imports...
from services import get_todos, get_uncompleted_todos

# Third-party imports...
from nose.tools import assert_is_not_none, assert_true, assert_list_equal
from unittest.mock import Mock, patch

def test_request_response():
    # Call the service, which will send a request to the server.
    response = get_todos()

    # If the request is sent successfully, then I expect a response to be returned.
    assert_is_not_none(response)

@patch('services.requests.get')
def test_getting_todos(mock_get):
    # Configure the mock to return a response with an OK status code.
    mock_get.return_value.ok = True
    
    # Call the service, which will send a request to the server.
    response = get_todos()
    
    # If the request is sent successfully, then I expect a response to be returned.
    assert_is_not_none(response)

def test_getting_todos1():
    mock_get_patcher = patch('services.requests.get')
    
    # Start patching `requests.get`.
    mock_get = mock_get_patcher.start()
    
    # Configure the mock to return a response with an OK status code.
    mock_get.return_value.ok = True

    # Call the service, which will send a request to the server.
    response = get_todos()
    
    # Stop patching `requests.get`.
    mock_get_patcher.stop()

    # If the request is sent successfully, then I expect a response to be returned.
    assert_is_not_none(response)

@patch('services.get_todos')
def test_getting_uncompleted_todos_when_todos_is_none(mock_get_todos):
    # configure mock to return none
    mock_get_todos.return_value = None
    
    # Call the service, which will return an empty list.
    uncompleted_todos = get_uncompleted_todos()

    # Confirm that the mock was called.
    assert_true(mock_get_todos.called)
    
    # Confirm that an empty list was returned.
    assert_list_equal(uncompleted_todos, [])

class TestTodos(object):
    @classmethod
    def setup_class(cls):
        cls.mock_get_patcher = patch('services.requests.get')
        cls.mock_get = cls.mock_get_patcher.start()
    
    @classmethod
    def teardown_class(cls):
        cls.mock_get_patcher.stop()
    
    def test_getting_todos_when_response_is_ok(self):
        # Configure the mock to return a response with an OK status code.
        self.mock_get.return_value.ok = True